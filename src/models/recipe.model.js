const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: {type: String, required: true},
    person: {type: Number, required: true},
    calories: {type: Number, required: true},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User'},
    createdAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Recipe',schema);