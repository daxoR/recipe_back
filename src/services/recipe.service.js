const db = require('../../config/db');
const Recipe = db.Recipe;

module.exports = {
    create: createRecipe,
    getAllRecipes,
    getRecipeById,
    updateRecipe,
    deleteRecipe
};

async function createRecipe(recipeParam) {
    if (await Recipe.findOne({name: recipeParam.name, person: recipeParam.person})) {
        throw 'Recipe ' + recipeParam.name + ' for ' + recipeParam.person + ' person already exist'
    }

    const recipe = new Recipe(recipeParam);
    await recipe.save();
}

async function getAllRecipes() {
    return await Recipe.find()
}

async function getRecipeById(id) {
    return await Recipe.findById(id);
}

async function updateRecipe(id, recipeParam) {
    const recipe = await Recipe.findById(id);

    //validate
    if (!recipe) throw "Recipe not found";
    if (await Recipe.findOne({name: recipeParam.name, person: recipeParam.person})){
        throw 'Recipe ' + recipeParam.name + ' for ' + recipeParam.person + ' person already exist'
    }

    // copy recipeParam properties to recipe
    Object.assign(recipe, recipeParam);

    await recipe.save();
}

async function deleteRecipe(id) {
    await Recipe.findByIdAndDelete(id);
}