class Helpers {

    static result(res, code_error, return_text, error_message = "") {
        try {
            if (res && code_error && return_text) {
                res.status(code_error);
                const code_status = code_error != 200 ? "error" : "success"
                res.send(
                    { 
                        meta: { status : code_status},
                        message: error_message,
                        data: {
                            return_text
                        }
                    }
                );
            } else {
                res.status(500);
                res.send(
                    { 
                        meta: { status : "error"},
                        message: "Can't get the result value, missing arguments",
                        data: {}
                    }
                );
            }
        } catch (e) {
            console.log("Wrong parameters : " + e.message);
        }
        
    }
}

module.exports = Helpers