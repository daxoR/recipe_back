const express = require('express');
const router = express.Router();
const recipeService = require('../services/recipe.service');

// Routes recipes
router.post('/create', create);
router.get('/', getAll);
router.get('/:id', getRecipeById);
router.put('/:id', updateRecipe);
router.delete('/:id', deleteRecipe);


module.exports = router;

function create(req, res, next) {
    recipeService.create(req.body)
        .then(() => res.json({}))
        .catch(err => {next(err)});
}

function getAll(req, res, next){
    recipeService.getAllRecipes()
        .then(recipes => res.json(recipes))
        .catch(err => {next(err)});
}

function getRecipeById(req, res, next){
    recipeService.getRecipeById(req.params.id)
        .then(recipe => res.json(recipe))
        .catch(err => {next(err)});
}

function updateRecipe(req, res, next){
    recipeService.updateRecipe(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => {next(err)});
}

function deleteRecipe(req, res, next){
    recipeService.deleteRecipe(req.params.id)
        .then(() => res.json({}))
        .catch(err => {next(err)})
}

