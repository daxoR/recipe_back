const mongoose = require('mongoose');
const mongodbUri = `mongodb://${process.env.MONGO_DB_ROOT_USERNAME}:${process.env.MONGO_DB_ROOT_PASSWORD}@localhost:${process.env.MONGO_DB_PORT}/${process.env.MONGO_DB_DATABASE}`
mongoose.connect(mongodbUri, { useCreateIndex: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../src/models/user.model'),
    Recipe: require('../src/models/recipe.model')
};